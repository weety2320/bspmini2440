#*********************************************************************************************************
# bspmini2440 Makefile
# target -> bspmini2440.elf
#           bspmini2440.bin
#*********************************************************************************************************

#*********************************************************************************************************
# include config.mk
#*********************************************************************************************************
CONFIG_MK_EXIST = $(shell if [ -f ../config.mk ]; then echo exist; else echo notexist; fi;)
ifeq ($(CONFIG_MK_EXIST), exist)
include ../config.mk
else
CONFIG_MK_EXIST = $(shell if [ -f config.mk ]; then echo exist; else echo notexist; fi;)
ifeq ($(CONFIG_MK_EXIST), exist)
include config.mk
else
CONFIG_MK_EXIST =
endif
endif

#*********************************************************************************************************
# check configure
#*********************************************************************************************************
check_defined = \
    $(foreach 1,$1,$(__check_defined))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $(value 2), ($(strip $2)))))

$(call check_defined, CONFIG_MK_EXIST, Please configure this project in RealCoder or \
create a config.mk file!)
$(call check_defined, SYLIXOS_BASE_PATH, SylixOS base project path)
$(call check_defined, TOOLCHAIN_PREFIX, the prefix name of toolchain)
$(call check_defined, DEBUG_LEVEL, debug level(debug or release))

#*********************************************************************************************************
# configure area you can set the following config to you own system
# FPUFLAGS (-mfloat-abi=softfp -mfpu=vfpv3 ...)
# CPUFLAGS (-mcpu=arm920t ...)
# NOTICE: libsylixos, BSP and other kernel modules projects CAN NOT use vfp!
#*********************************************************************************************************
FPUFLAGS =
CPUFLAGS = -mcpu=arm920t

#*********************************************************************************************************
# toolchain select
#*********************************************************************************************************
CC  = $(TOOLCHAIN_PREFIX)gcc
CXX = $(TOOLCHAIN_PREFIX)g++
AS  = $(TOOLCHAIN_PREFIX)gcc
AR  = $(TOOLCHAIN_PREFIX)ar
LD  = $(TOOLCHAIN_PREFIX)g++
OC  = $(TOOLCHAIN_PREFIX)objcopy
SZ  = $(TOOLCHAIN_PREFIX)size

#*********************************************************************************************************
# symbol.c symbol.h path
#*********************************************************************************************************
SYMBOL_PATH = SylixOS/bsp

#*********************************************************************************************************
# do not change the following code
# buildin internal application source
#*********************************************************************************************************

#*********************************************************************************************************
# symbol.c
#*********************************************************************************************************
SYM_SRCS = $(SYMBOL_PATH)/symbol.c

#*********************************************************************************************************
# bsp src(s) file
#*********************************************************************************************************
BSP_SRCS = \
SylixOS/bsp/bspInit.c \
SylixOS/bsp/bspLib.c \
SylixOS/bsp/startup.S \
SylixOS/bsp/targetInit.c

#*********************************************************************************************************
# drivers src(s) file
#*********************************************************************************************************
DRV_SRCS = \
SylixOS/driver/dma/samsungdma.c \
SylixOS/driver/gpio/s3c2440_gpio.c \
SylixOS/driver/i2c/samsungi2c.c \
SylixOS/driver/lcd/s3c2440a_lcd.c \
SylixOS/driver/mtd/nand/k9f1g08.c \
SylixOS/driver/mtd/nand/nand.c \
SylixOS/driver/mtd/nand/s3c24xx_nand.c \
SylixOS/driver/netif/dm9000x.c \
SylixOS/driver/pm/s3c2440a_pm.c \
SylixOS/driver/rtc/rtc.c \
SylixOS/driver/sdi/mciLib.c \
SylixOS/driver/sdi/s3csdi.c \
SylixOS/driver/sdi/sdInit.c \
SylixOS/driver/timer/timer.c \
SylixOS/driver/touchscr/s3c_onewire.c \
SylixOS/driver/touchscr/touchscr.c \
SylixOS/driver/tty/samsungtty.c \
SylixOS/driver/tty/uart.c

#*********************************************************************************************************
# user src(s) file
#*********************************************************************************************************
USR_SRCS = \
SylixOS/user/main.c

#*********************************************************************************************************
# all bspmini2440 source
#*********************************************************************************************************
SRCS  = $(BSP_SRCS)
SRCS += $(DRV_SRCS)
SRCS += $(USR_SRCS)
SRCS += $(SYM_SRCS)

#*********************************************************************************************************
# build path
#*********************************************************************************************************
ifeq ($(DEBUG_LEVEL), debug)
OUTDIR = Debug
else
OUTDIR = Release
endif

OUTPATH = ./$(OUTDIR)
OBJPATH = $(OUTPATH)/obj
DEPPATH = $(OUTPATH)/dep

#*********************************************************************************************************
# target
#*********************************************************************************************************
O_IMG = $(OUTPATH)/bspmini2440.elf
O_BIN = $(OUTPATH)/bspmini2440.bin
O_SIZ = $(OUTPATH)/bspmini2440.siz

#*********************************************************************************************************
# bspmini2440 objects
#*********************************************************************************************************
OBJS = $(addprefix $(OBJPATH)/, $(addsuffix .o, $(basename $(SRCS))))
DEPS = $(addprefix $(DEPPATH)/, $(addsuffix .d, $(basename $(SRCS))))

#*********************************************************************************************************
# include path
#*********************************************************************************************************
INCDIR  = -I"$(SYLIXOS_BASE_PATH)/libsylixos/SylixOS"
INCDIR += -I"$(SYLIXOS_BASE_PATH)/libsylixos/SylixOS/include"
INCDIR += -I"$(SYLIXOS_BASE_PATH)/libsylixos/SylixOS/include/inet"

INCDIR += -I"./SylixOS"
INCDIR += -I"./SylixOS/bsp"

#*********************************************************************************************************
# compiler preprocess
#*********************************************************************************************************
DSYMBOL  = -DSYLIXOS
DSYMBOL += -D__BOOT_INRAM=1

#*********************************************************************************************************
# load script
#*********************************************************************************************************
LD_SCRIPT = SylixOSBSP.ld

#*********************************************************************************************************
# depend dynamic library
#*********************************************************************************************************
DEPEND_DLL = -lsylixos

#*********************************************************************************************************
# depend dynamic library search path
#*********************************************************************************************************
DEPEND_DLL_PATH = -L"$(SYLIXOS_BASE_PATH)/libsylixos/$(OUTDIR)"

#*********************************************************************************************************
# compiler optimize
#*********************************************************************************************************
ifeq ($(DEBUG_LEVEL), debug)
OPTIMIZE = -O0 -g3 -gdwarf-2
else
OPTIMIZE = -O2 -g1 -gdwarf-2											# Do NOT use -O3 and -Os
endif										    						# -Os is not align for function
																		# loop and jump.
#*********************************************************************************************************
# depends and compiler parameter (cplusplus in kernel MUST NOT use exceptions and rtti)
#*********************************************************************************************************
DEPENDFLAG  = -MM
CXX_EXCEPT  = -fno-exceptions -fno-rtti
COMMONFLAGS = $(CPUFLAGS) $(OPTIMIZE) -Wall -fmessage-length=0 -fsigned-char -fno-short-enums
ASFLAGS     = -x assembler-with-cpp $(DSYMBOL) $(INCDIR) $(COMMONFLAGS) -c
CFLAGS      = $(DSYMBOL) $(INCDIR) $(COMMONFLAGS) -c
CXXFLAGS    = $(DSYMBOL) $(INCDIR) $(CXX_EXCEPT) $(COMMONFLAGS) -c
ARFLAGS     = -r

#*********************************************************************************************************
# define some useful variable
#*********************************************************************************************************
DEPEND          = $(CC)  $(DEPENDFLAG) $(CFLAGS)
DEPEND.d        = $(subst -g ,,$(DEPEND))
COMPILE.S       = $(AS)  $(ASFLAGS)
COMPILE_VFP.S   = $(AS)  $(ASFLAGS) $(FPUFLAGS)
COMPILE.c       = $(CC)  $(CFLAGS)
COMPILE.cxx     = $(CXX) $(CXXFLAGS)

#*********************************************************************************************************
# target
#*********************************************************************************************************
all: $(O_IMG)
		@echo create "$(O_IMG) $(O_BIN)" success.

#*********************************************************************************************************
# include depends
#*********************************************************************************************************
ifneq ($(MAKECMDGOALS), clean)
ifneq ($(MAKECMDGOALS), clean_project)
sinclude $(DEPS)
endif
endif

#*********************************************************************************************************
# auto copy symbol.c symbol.h
#*********************************************************************************************************
EMPTY=
SPACE= $(EMPTY) $(EMPTY)
$(SYMBOL_PATH)/symbol.c:$(subst $(SPACE),\ ,$(SYLIXOS_BASE_PATH))/libsylixos/$(OUTDIR)/symbol.c
		cp "$(SYLIXOS_BASE_PATH)/libsylixos/$(OUTDIR)/symbol.c" $(SYMBOL_PATH)/symbol.c
		cp "$(SYLIXOS_BASE_PATH)/libsylixos/$(OUTDIR)/symbol.h" $(SYMBOL_PATH)/symbol.h

#*********************************************************************************************************
# create depends files
#*********************************************************************************************************
$(DEPPATH)/%.d: %.c
		@echo creating $@
		@if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
		@rm -f $@; \
		echo -n '$@ $(addprefix $(OBJPATH)/, $(dir $<))' > $@; \
		$(DEPEND.d) $< >> $@ || rm -f $@; exit;

$(DEPPATH)/%.d: %.cpp
		@echo creating $@
		@if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
		@rm -f $@; \
		echo -n '$@ $(addprefix $(OBJPATH)/, $(dir $<))' > $@; \
		$(DEPEND.d) $< >> $@ || rm -f $@; exit;

#*********************************************************************************************************
# compile source files
#*********************************************************************************************************
$(OBJPATH)/%.o: %.S
		@if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
		$(COMPILE.S) $< -o $@

$(OBJPATH)/%.o: %.c
		@if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
		$(COMPILE.c) $< -o $@

$(OBJPATH)/%.o: %.cpp
		@if [ ! -d "$(dir $@)" ]; then mkdir -p "$(dir $@)"; fi
		$(COMPILE.cxx) $< -o $@

#*********************************************************************************************************
# link bspmini2440.elf object files
#*********************************************************************************************************
$(O_IMG): $(OBJS) $(LD_SCRIPT)
		$(LD) $(CPUFLAGS) -nostdlib $(addprefix -T,$(LD_SCRIPT)) -o $(O_IMG) $(OBJS) \
		$(DEPEND_DLL_PATH) $(DEPEND_DLL) -lm -lgcc
		$(OC) -O binary $(O_IMG) $(O_BIN)
		$(SZ) --format=berkeley $(O_IMG) > $(O_SIZ)

#*********************************************************************************************************
# clean
#*********************************************************************************************************
.PHONY: clean
.PHONY: clean_project

#*********************************************************************************************************
# clean objects
#*********************************************************************************************************
clean:
		-rm -rf $(O_SIZ)
		-rm -rf $(O_IMG)
		-rm -rf $(O_BIN)
		-rm -rf $(SYMBOL_PATH)/symbol.c $(SYMBOL_PATH)/symbol.h
		-rm -rf $(OBJPATH)
		-rm -rf $(DEPPATH)

#*********************************************************************************************************
# clean project
#*********************************************************************************************************
clean_project:
		-rm -rf $(OUTPATH)

#*********************************************************************************************************
# END
#*********************************************************************************************************
