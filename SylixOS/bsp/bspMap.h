/**********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                       SylixOS(TM)
**
**                               Copyright  All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: bspMap.h
**
** 创   建   人: Han.Hui (韩辉)
**
** 文件创建日期: 2008 年 12 月 23 日
**
** 描        述: C 程序入口部分. 物理分页空间与全局映射关系表定义.
*********************************************************************************************************/

#ifndef __BSPMAP_H
#define __BSPMAP_H

/*********************************************************************************************************
   内存分配关系图

    +-----------------------+--------------------------------+
    |          24M          |             40M                |
    |       通用内存区      |          VMM 管理区            |
    |         CACHE         |                                |
    +-----------------------+--------------------------------+
    3000_0000               3180_0000                        33FF_FFFF

*********************************************************************************************************/

/*********************************************************************************************************
  physical memory zone
*********************************************************************************************************/
#ifdef  __BSPINIT_MAIN_FILE

LW_VMM_ZONE_DESC    _G_zonedescGlobal[] = {
    {0x31800000, 0x01400000, LW_ZONE_ATTR_DMA},                         /*  均可被 DMA 使用             */
    {0x32C00000, 0x01400000, LW_ZONE_ATTR_DMA},
};

/*********************************************************************************************************
  初始化内存类型与布局 (VMM 管理区的内存不需要在全局初始化表中初始化)
*********************************************************************************************************/

LW_MMU_GLOBAL_DESC  _G_globaldescMap[] = {

#ifdef __BOOT_INRAM
    /*
     *  ARM except table
     */
    {                                                                   /*  中断向量表                  */
        0x00000000,                                                     /*  ARM except table            */
        0x30000000,                                                     /*  RAM except table            */
        LW_CFG_VMM_PAGE_SIZE,                                           /*  one page size               */
        (LW_VMM_FLAG_EXEC)                                              /*  文字池类型                  */
    },
    /*
     *  nor-flash area
     */
    {
        (0x00000000 + LW_CFG_VMM_PAGE_SIZE),                            /*  flash 剩下的空间直接映射    */
        (0x00000000 + LW_CFG_VMM_PAGE_SIZE),
        ((2 * LW_CFG_MB_SIZE) - LW_CFG_VMM_PAGE_SIZE),
        (LW_VMM_FLAG_EXEC)                                              /*  文字池类型                  */
    },
#else                                                                   /*  __DEBUG_IN_FLASH            */
    /*
     *  nor-flash area
     */
    {
        (0x00000000),                                                   /*  flash 直接映射              */
        (0x00000000),
        (2 * LW_CFG_MB_SIZE),
        (LW_VMM_FLAG_EXEC)                                              /*  文字池类型                  */
    },
#endif                                                                  /*  __DEBUG_IN_RAM              */
    
    /*
     *  kernel space
     */
    {                                                                   /*  BANK6 - SDRAM 6MByte        */
        (0x30000000),
        (0x30000000),
        (6 * 1024 * 1024),                                              /*  SylixOS Kernel Text         */
        (LW_VMM_FLAG_EXEC | LW_VMM_FLAG_RDWR)                           /*  此段可修改, 方便调试        */
    },
    
    {                                                                   /*  BANK6 - SDRAM 18MByte       */
        (0x30600000),
        (0x30600000),
        (18 * 1024 * 1024),                                             /*  SylixOS Kernel Memory       */
        (LW_VMM_FLAG_RDWR)                                              /*  状态属性 CB                 */
    },
    
    /*
     *  internal sfr area
     */
    {                                                                   /*  memory controller           */
        0x48000000,
        0x48000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  USB HOST controller         */
        0x49000000,
        0x49000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  INTERRUPT controller        */
        0x4a000000,
        0x4a000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  DMA controller              */
        0x4b000000,
        0x4b000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  CLOCK & POWER controller    */
        0x4c000000,
        0x4c000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  LCD controller              */
        0x4d000000,
        0x4d000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  NAND FLASH controller       */
        0x4E000000,
        0x4E000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  CAMERA controller           */
        0x4F000000,
        0x4F000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  UART0 controller            */
        0x50000000,
        0x50000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  UART1 controller            */
        0x50004000,
        0x50004000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  UART2 controller            */
        0x50008000,
        0x50008000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  PWM TIMER controller        */
        0x51000000,
        0x51000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  USB DEV controller          */
        0x52000000,
        0x52000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  WATCH DOG TIMER controller  */
        0x53000000,
        0x53000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  IIC controller              */
        0x54000000,
        0x54000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  IIS controller              */
        0x55000000,
        0x55000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  I/O PORT  controller        */
        0x56000000,
        0x56000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  RTC  controller             */
        0x57000000,
        0x57000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  A/DC  controller            */
        0x58000000,
        0x58000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  SPI  controller             */
        0x59000000,
        0x59000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  SD Interface  controller    */
        0x5a000000,
        0x5a000000,
        LW_CFG_VMM_PAGE_SIZE,
        (LW_VMM_FLAG_DMA)                                               /*  状态属性 NCNB               */
    },
    
    {                                                                   /*  结束                        */
        0,
        0,
        0,
        0
    }
};
#endif                                                                  /*  __BSPINIT_MAIN_FILE         */
#endif                                                                  /*  __BSPMAP_H                  */
/*********************************************************************************************************
  END
*********************************************************************************************************/
