/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                SylixOS(TM)  LW : long wing
**
**                               Copyright All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: dm9000x.c
**
** 创   建   人: JiaoJinXing
**
** 文件创建日期: 2012 年 03 月 03 日
**
** 描        述: DM9000X 以太网芯片驱动.
*********************************************************************************************************/
#define  __SYLIXOS_KERNEL
#include "SylixOS.h"
#include "netif/etharp.h"
#include "lwip/ethip6.h"
#include "lwip/netif.h"
#include "lwip/sys.h"
#include "lwip/stats.h"
#include "lwip/snmp.h"
#include "dm9000x.h"
/*********************************************************************************************************
** DM9000 驱动配置
*********************************************************************************************************/
#define LINK_SNMP_STAT          1
#define DM9000_WATCHDOG_CNT     (LW_TICK_HZ * 1)                        /* 每 1 秒检查一次网线连接状态  */
/*********************************************************************************************************
** DM9000 寄存器定义
*********************************************************************************************************/
#define DM9000_ID               0x90000A46
#define DM9000_PKT_MAX          1536                                    /*  Received packet max size    */
#define DM9000_PKT_RDY          0x01                                    /*  Packet ready to receive     */
#define DM9000_PKT_ERR          0x02
/*
 * although the registers are 16 bit, they are 32-bit aligned.
 */
#define DM9000_NCR              0x00
#define DM9000_NSR              0x01
#define DM9000_TCR              0x02
#define DM9000_TSR1             0x03
#define DM9000_TSR2             0x04
#define DM9000_RCR              0x05
#define DM9000_RSR              0x06
#define DM9000_ROCR             0x07
#define DM9000_BPTR             0x08
#define DM9000_FCTR             0x09
#define DM9000_FCR              0x0A
#define DM9000_EPCR             0x0B
#define DM9000_EPAR             0x0C
#define DM9000_EPDRL            0x0D
#define DM9000_EPDRH            0x0E
#define DM9000_WCR              0x0F

#define DM9000_PAR              0x10
#define DM9000_MAR              0x16

#define DM9000_GPCR             0x1e
#define DM9000_GPR              0x1f
#define DM9000_TRPAL            0x22
#define DM9000_TRPAH            0x23
#define DM9000_RWPAL            0x24
#define DM9000_RWPAH            0x25

#define DM9000_VIDL             0x28
#define DM9000_VIDH             0x29
#define DM9000_PIDL             0x2A
#define DM9000_PIDH             0x2B

#define DM9000_CHIPR            0x2C
#define DM9000_SMCR             0x2F

#define DM9000_PHY              0x40                                    /*  PHY address 0x01            */

#define DM9000_MRCMDX           0xF0
#define DM9000_MRCMD            0xF2
#define DM9000_MRRL             0xF4
#define DM9000_MRRH             0xF5
#define DM9000_MWCMDX           0xF6
#define DM9000_MWCMD            0xF8
#define DM9000_MWRL             0xFA
#define DM9000_MWRH             0xFB
#define DM9000_TXPLL            0xFC
#define DM9000_TXPLH            0xFD
#define DM9000_ISR              0xFE
#define DM9000_IMR              0xFF

#define NCR_EXT_PHY             (1 << 7)
#define NCR_WAKEEN              (1 << 6)
#define NCR_FCOL                (1 << 4)
#define NCR_FDX                 (1 << 3)
#define NCR_LBK                 (3 << 1)
#define NCR_LBK_INT_MAC         (1 << 1)
#define NCR_LBK_INT_PHY         (2 << 1)
#define NCR_RST                 (1 << 0)

#define NSR_SPEED               (1 << 7)
#define NSR_LINKST              (1 << 6)
#define NSR_WAKEST              (1 << 5)
#define NSR_TX2END              (1 << 3)
#define NSR_TX1END              (1 << 2)
#define NSR_RXOV                (1 << 1)

#define TCR_TJDIS               (1 << 6)
#define TCR_EXCECM              (1 << 5)
#define TCR_PAD_DIS2            (1 << 4)
#define TCR_CRC_DIS2            (1 << 3)
#define TCR_PAD_DIS1            (1 << 2)
#define TCR_CRC_DIS1            (1 << 1)
#define TCR_TXREQ               (1 << 0)

#define TSR_TJTO                (1 << 7)
#define TSR_LC                  (1 << 6)
#define TSR_NC                  (1 << 5)
#define TSR_LCOL                (1 << 4)
#define TSR_COL                 (1 << 3)
#define TSR_EC                  (1 << 2)

#define RCR_WTDIS               (1 << 6)
#define RCR_DIS_LONG            (1 << 5)
#define RCR_DIS_CRC             (1 << 4)
#define RCR_ALL                 (1 << 3)
#define RCR_RUNT                (1 << 2)
#define RCR_PRMSC               (1 << 1)
#define RCR_RXEN                (1 << 0)

#define RSR_RF                  (1 << 7)
#define RSR_MF                  (1 << 6)
#define RSR_LCS                 (1 << 5)
#define RSR_RWTO                (1 << 4)
#define RSR_PLE                 (1 << 3)
#define RSR_AE                  (1 << 2)
#define RSR_CE                  (1 << 1)
#define RSR_FOE                 (1 << 0)

#define EPCR_EPOS_PHY           (1 << 3)
#define EPCR_EPOS_EE            (0 << 3)
#define EPCR_ERPRR              (1 << 2)
#define EPCR_ERPRW              (1 << 1)
#define EPCR_ERRE               (1 << 0)

#define FCTR_HWOT(ot)           ((ot & 0xF) << 4)
#define FCTR_LWOT(ot)           (ot & 0xF)

#define BPTR_BPHW(x)            ((x) << 4)
#define BPTR_JPT_200US          (0x07)
#define BPTR_JPT_600US          (0x0F)

#define IMR_PAR                 (1 << 7)
#define IMR_ROOM                (1 << 3)
#define IMR_ROM                 (1 << 2)
#define IMR_PTM                 (1 << 1)
#define IMR_PRM                 (1 << 0)

#define ISR_ROOS                (1 << 3)
#define ISR_ROS                 (1 << 2)
#define ISR_PTS                 (1 << 1)
#define ISR_PRS                 (1 << 0)
#define ISR_CLR_STATUS          (ISR_ROOS | ISR_ROS | ISR_PTS | ISR_PRS)

#define GPCR_GPIO0_OUT          (1 << 0)

#define GPR_PHY_PWROFF          (1 << 0)
/*********************************************************************************************************
** 网络接口私有数据定义
*********************************************************************************************************/
/*
 * Helper struct to hold private data used to operate your ethernet interface.
 * Keeping the ethernet address of the MAC in this struct is not necessary
 * as it is already kept in the struct netif.
 * But this is only an example, anyway...
 */
struct dm9000_netif_priv {
    atomic_t                    tx_pkt_cnt;
    u16_t                       queue_pkt_len;
    LW_OBJECT_HANDLE            lock;
    LW_OBJECT_HANDLE            tx_sync;
    LW_OBJECT_HANDLE            watchdog;
    int                         irq;
    struct dm9000_data          dm9000;
    void (*outblk)(struct dm9000_data *dm9000, const void *data, int len);
    void (*inblk)(struct dm9000_data *dm9000, void *data, int len);
    void (*dummy_inblk)(struct dm9000_data *dm9000, int len);
    void (*rx_status)(struct dm9000_data *dm9000, u16_t *status, u16_t *len);
};

/*
 * Define those to better describe your network interface.
 */
#define IFNAME0                 'e'
#define IFNAME1                 'n'

#define HOSTNAME                "lwIP"

/*
 * ARCH IO 级
 */
#define __le16_to_cpu(x)        ((u16_t)(x))

static void dm9000_watchdog(struct netif *netif);
/*********************************************************************************************************
** Function name:           dm9000_outblk_8bit
** Descriptions:            输出块 8 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_outblk_8bit(struct dm9000_data *dm9000, const u8_t *data, int len)
{
    writes8(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_outblk_16bit
** Descriptions:            输出块 16 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_outblk_16bit(struct dm9000_data *dm9000, const u16_t *data, int len)
{
    len = (len + 1) / 2;

    writes16(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_outblk_32bit
** Descriptions:            输出块 32 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_outblk_32bit(struct dm9000_data *dm9000, const u32_t *data, int len)
{
    len = (len + 3) / 4;

    writes32(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_inblk_8bit
** Descriptions:            输入块 8 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_inblk_8bit(struct dm9000_data *dm9000, u8_t *data, int len)
{
    reads8(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_dummy_inblk_8bit
** Descriptions:            输入块 8 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_dummy_inblk_8bit(struct dm9000_data *dm9000, int len)
{
    int  i;
    u8_t data;

    (void)data;

    for (i = 0; i < len; i++) {
        data = read8(dm9000->uiDataAddr);
    }
}
/*********************************************************************************************************
** Function name:           dm9000_inblk_16bit
** Descriptions:            输入块 16 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_inblk_16bit(struct dm9000_data *dm9000, u16_t *data, int len)
{
    len = (len + 1) / 2;

    reads16(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_dummy_inblk_16bit
** Descriptions:            输入块 16 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_dummy_inblk_16bit(struct dm9000_data *dm9000, int len)
{
    int  i;
    u16_t data;

    (void)data;

    len = (len + 1) / 2;

    for (i = 0; i < len; i++) {
        data = read16(dm9000->uiDataAddr);
    }
}
/*********************************************************************************************************
** Function name:           dm9000_inblk_32bit
** Descriptions:            输入块 32 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_inblk_32bit(struct dm9000_data *dm9000, u32_t *data, int len)
{
    len = (len + 3) / 4;

    reads32(dm9000->uiDataAddr, data, len);
}
/*********************************************************************************************************
** Function name:           dm9000_dummy_inblk_32bit
** Descriptions:            输入块 32 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_dummy_inblk_32bit(struct dm9000_data *dm9000, int len)
{
    int  i;
    u32_t data;

    (void)data;

    len = (len + 3) / 4;

    for (i = 0; i < len; i++) {
        data = read32(dm9000->uiDataAddr);
    }
}
/*********************************************************************************************************
** Function name:           dm9000_rx_status_32bit
** Descriptions:            获得接收状态 32 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_rx_status_32bit(struct dm9000_data *dm9000, u16_t *status, u16_t *len)
{
    u32_t  temp;

    write8(DM9000_MRCMD, dm9000->uiIoAddr);

    temp    = read32(dm9000->uiDataAddr);
    *status = __le16_to_cpu(temp);
    *len    = __le16_to_cpu(temp >> 16);
}
/*********************************************************************************************************
** Function name:           dm9000_rx_status_16bit
** Descriptions:            获得接收状态 16 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_rx_status_16bit(struct dm9000_data *dm9000, u16_t *status, u16_t *len)
{
    write8(DM9000_MRCMD, dm9000->uiIoAddr);

    *status = __le16_to_cpu(read16(dm9000->uiDataAddr));
    *len    = __le16_to_cpu(read16(dm9000->uiDataAddr));
}
/*********************************************************************************************************
** Function name:           dm9000_rx_status_8bit
** Descriptions:            获得接收状态 8 位模式
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_rx_status_8bit(struct dm9000_data *dm9000, u16_t *status, u16_t *len)
{
    write8(DM9000_MRCMD, dm9000->uiIoAddr);

    *status = __le16_to_cpu(read8(dm9000->uiDataAddr) + (read8(dm9000->uiDataAddr) << 8));
    *len    = __le16_to_cpu(read8(dm9000->uiDataAddr) + (read8(dm9000->uiDataAddr) << 8));
}
/*********************************************************************************************************
** Function name:           dm9000_io_read
** Descriptions:            从 IO 端口读一个字节
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static u8_t dm9000_io_read(struct dm9000_data *dm9000, u8_t reg)
{
    write8(reg, dm9000->uiIoAddr);
    return read8(dm9000->uiDataAddr);
}
/*********************************************************************************************************
** Function name:           dm9000_io_write
** Descriptions:            写一个字节到 IO 端口
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_io_write(struct dm9000_data *dm9000, u8_t reg, u8_t value)
{
    write8(reg, dm9000->uiIoAddr);
    write8(value, dm9000->uiDataAddr);
}
/*********************************************************************************************************
** Function name:           dm9000_phy_read
** Descriptions:            Read a word from phyxcer
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static u16_t dm9000_phy_read(struct dm9000_data *dm9000, u8_t reg)
{
    u16_t  value;

    /*
     * Fill the phyxcer register into REG_0C
     */
    dm9000_io_write(dm9000, DM9000_EPAR, DM9000_PHY | reg);

    dm9000_io_write(dm9000, DM9000_EPCR, 0x0C);                         /*  Issue phyxcer read command  */
    bspDelayUs(100);                                                    /*  Wait read complete          */
    dm9000_io_write(dm9000, DM9000_EPCR, 0x00);                         /*  Clear phyxcer read command  */

    value = (dm9000_io_read(dm9000, DM9000_EPDRH) << 8) |
             dm9000_io_read(dm9000, DM9000_EPDRL);

    return value;
}
/*********************************************************************************************************
** Function name:           dm9000_phy_write
** Descriptions:            Write a word to phyxcer
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
#if 0                                                                   /* No use                       */
static void dm9000_phy_write(struct dm9000_data *dm9000, u8_t reg, u16_t value)
{
    /*
     * Fill the phyxcer register into REG_0C
     */
    dm9000_io_write(dm9000, DM9000_EPAR, DM9000_PHY | reg);

    /*
     * Fill the written data into REG_0D & REG_0E
     */
    dm9000_io_write(dm9000, DM9000_EPDRL, (value & 0xFF));
    dm9000_io_write(dm9000, DM9000_EPDRH, ((value >> 8) & 0xFF));

    dm9000_io_write(dm9000, DM9000_EPCR, 0x0A);                         /*  Issue phyxcer write command */
    bspDelayUs(500);                                                    /*  Wait write complete         */
    dm9000_io_write(dm9000, DM9000_EPCR, 0x00);                         /*  Clear phyxcer write command */
}
#endif
/*********************************************************************************************************
** Function name:           dm9000_probe
** Descriptions:            探测 DM9000 芯片
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static err_t dm9000_probe(struct dm9000_data *dm9000)
{
    u32_t  id;

    id  = dm9000_io_read(dm9000, DM9000_VIDL);
    id |= dm9000_io_read(dm9000, DM9000_VIDH) << 8;
    id |= dm9000_io_read(dm9000, DM9000_PIDL) << 16;
    id |= dm9000_io_read(dm9000, DM9000_PIDH) << 24;

    if (id == DM9000_ID) {
        printk(KERN_INFO "dm9000_probe: found at i/o: 0x%x, id: 0x%x\n", dm9000->uiBaseAddr, id);
        return 0;
    } else {
        printk(KERN_ERR "dm9000_probe: no found at i/o: 0x%x, id: 0x%x\n", dm9000->uiBaseAddr, id);
        return -1;
    }
}
/*********************************************************************************************************
** Function name:           dm9000_reset
** Descriptions:            复位 DM9000 芯片
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static err_t dm9000_reset(struct dm9000_data *dm9000)
{
    printk(KERN_INFO "dm9000_reset: resetting\n");

    /*
     * Reset DM9000,
     * see DM9000 Application Notes V1.22 Jun 11, 2004 page 29
     */

    /*
     * DEBUG: Make all GPIO0 outputs, all others inputs
     */
    dm9000_io_write(dm9000, DM9000_GPCR, 0x0f);

    /*
     * Step 1: Power internal PHY by writing 0 to GPIO0 pin
     */
    dm9000_io_write(dm9000, DM9000_GPR, 0x00);

    /*
     * Step 2: Software reset
     */
    dm9000_io_write(dm9000, DM9000_NCR, (NCR_LBK_INT_MAC | NCR_RST));

    do {
        printk(KERN_INFO "dm9000_reset: 1st reset\n");
        usleep(25);                                                     /*  Wait at least 20 us         */
    } while (dm9000_io_read(dm9000, DM9000_NCR) & 1);

    dm9000_io_write(dm9000, DM9000_NCR, 0x00);
    dm9000_io_write(dm9000, DM9000_NCR, (NCR_LBK_INT_MAC | NCR_RST));   /*  Issue a second reset        */

    do {
        printk(KERN_INFO "dm9000_reset: 2st reset\n");
        usleep(25);                                                     /*  Wait at least 20 us         */
    } while (dm9000_io_read(dm9000, DM9000_NCR) & 1);

    /*
     * Check whether the ethernet controller is present
     */
    if ((dm9000_io_read(dm9000, DM9000_PIDL) != 0x00) ||
        (dm9000_io_read(dm9000, DM9000_PIDH) != 0x90)) {
        printk(KERN_ERR "dm9000_reset: no respond\n");
        return -1;
    }

    return 0;
}
/*********************************************************************************************************
** Function name:           dm9000_init
** Descriptions:            初始化 DM9000 芯片
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static err_t dm9000_init(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    u8_t                       mode, oft;
    err_t                      ret;
    int                        i;
    u16_t                      hash_table[4];

    /*
     * 复位 DM9000 芯片
     */
    ret = dm9000_reset(dm9000);
    if (ret < 0) {
        printk(KERN_ERR "dm9000_init: reset error\n");
        return -1;
    }

    /*
     * 探测 DM9000 芯片
     */
    ret = dm9000_probe(dm9000);
    if (ret < 0) {
        printk(KERN_ERR "dm9000_init: probe error\n");
        return -1;
    }

    /*
     * Auto-detect 8/16/32 bit mode, ISR Bit 6+7 indicate bus width
     */
    mode = dm9000_io_read(dm9000, DM9000_ISR) >> 6;
    switch (mode) {
    case 0x00:  /* 16-bit mode */
        printk(KERN_INFO "dm9000_init: running in 16 bit mode\n");
        netif_priv->outblk      = (void (*)(struct dm9000_data *, const void *, int))dm9000_outblk_16bit;
        netif_priv->inblk       = (void (*)(struct dm9000_data *, void *, int))dm9000_inblk_16bit;
        netif_priv->dummy_inblk = dm9000_dummy_inblk_16bit;
        netif_priv->rx_status   = dm9000_rx_status_16bit;
        break;

    case 0x01:  /* 32-bit mode */
        printk(KERN_INFO "dm9000_init: running in 32 bit mode\n");
        netif_priv->outblk      = (void (*)(struct dm9000_data *, const void *, int))dm9000_outblk_32bit;
        netif_priv->inblk       = (void (*)(struct dm9000_data *, void *, int))dm9000_inblk_32bit;
        netif_priv->dummy_inblk = dm9000_dummy_inblk_32bit;
        netif_priv->rx_status   = dm9000_rx_status_32bit;
        break;

    case 0x02:  /* 8 bit mode */
        printk(KERN_INFO "dm9000_init: running in 8 bit mode\n");
        netif_priv->outblk      = (void (*)(struct dm9000_data *, const void *, int))dm9000_outblk_8bit;
        netif_priv->inblk       = (void (*)(struct dm9000_data *, void *, int))dm9000_inblk_8bit;
        netif_priv->dummy_inblk = dm9000_dummy_inblk_8bit;
        netif_priv->rx_status   = dm9000_rx_status_8bit;
        break;

    default:    /* Assume 8 bit mode, will probably not work anyway */
        printk(KERN_INFO "dm9000_init: running in 0x%02x bit mode\n", mode);
        netif_priv->outblk      = (void (*)(struct dm9000_data *, const void *, int))dm9000_outblk_8bit;
        netif_priv->inblk       = (void (*)(struct dm9000_data *, void *, int))dm9000_inblk_8bit;
        netif_priv->dummy_inblk = dm9000_dummy_inblk_8bit;
        netif_priv->rx_status   = dm9000_rx_status_8bit;
        break;
    }

    /*
     * Program operating register, only internal phy supported
     */
    dm9000_io_write(dm9000, DM9000_NCR, 0x00);

    /*
     * TX Polling clear
     */
    dm9000_io_write(dm9000, DM9000_TCR, 0x00);

    /*
     * Less 3Kb, 200us
     */
    dm9000_io_write(dm9000, DM9000_BPTR, BPTR_BPHW(3) | BPTR_JPT_600US);

    /*
     * Flow Control: High/Low Water
     */
    dm9000_io_write(dm9000, DM9000_FCTR, FCTR_HWOT(3) | FCTR_LWOT(8));

    /*
     * TODO: This looks strange! Flow Control
     */
    dm9000_io_write(dm9000, DM9000_FCR, 0xff);

    /*
     * Special Mode
     */
    dm9000_io_write(dm9000, DM9000_SMCR, 0x00);

    /*
     * Clear TX status
     */
    dm9000_io_write(dm9000, DM9000_NSR, NSR_WAKEST | NSR_TX2END | NSR_TX1END);

    /*
     * Clear interrupt status
     */
    dm9000_io_write(dm9000, DM9000_ISR, ISR_CLR_STATUS);

    /*
     * Fill device MAC address registers
     */
    for (i = 0, oft = DM9000_PAR; i < 6; i++, oft++) {
        dm9000_io_write(dm9000, oft, netif->hwaddr[i]);
    }

    /*
     * Clear Hash Table
     */
    for (i = 0; i < 4; i++) {
        hash_table[i] = 0x0;
    }

    /*
     * broadcast address
     */
    hash_table[3] = 0x8000;

    for (i = 0, oft = DM9000_MAR; i < 4; i++) {
        dm9000_io_write(dm9000, oft++, hash_table[i]);
        dm9000_io_write(dm9000, oft++, hash_table[i] >> 8);
    }

    /*
     * Read back MAC address
     */
    printk(KERN_INFO "dm9000_init: MAC=");
    printk(KERN_INFO "%02x:", dm9000_io_read(dm9000, DM9000_PAR + 0));
    printk(KERN_INFO "%02x:", dm9000_io_read(dm9000, DM9000_PAR + 1));
    printk(KERN_INFO "%02x:", dm9000_io_read(dm9000, DM9000_PAR + 2));
    printk(KERN_INFO "%02x:", dm9000_io_read(dm9000, DM9000_PAR + 3));
    printk(KERN_INFO "%02x:", dm9000_io_read(dm9000, DM9000_PAR + 4));
    printk(KERN_INFO "%02x\n",dm9000_io_read(dm9000, DM9000_PAR + 5));

    /*
     * Activate DM9000
     */
    /*
     * RX enable
     */
    dm9000_io_write(dm9000, DM9000_RCR, RCR_DIS_LONG | RCR_DIS_CRC | RCR_RXEN);

    /*
     * Enable TX/RX interrupt mask
     */
    dm9000_io_write(dm9000, DM9000_IMR, IMR_PAR | IMR_ROOM | IMR_ROM | IMR_PTM | IMR_PRM);

    return 0;
}
/*********************************************************************************************************
** Function name:           dm9000_drop
** Descriptions:            DM9000 接收数据
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_drop(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    u8_t                       rx_byte;
    u16_t                      rx_len;
    u16_t                      rx_status;

    do {
        dm9000_io_read(dm9000, DM9000_MRCMDX);
        rx_byte = read8(dm9000->uiDataAddr) & 0x03;

        if (rx_byte & DM9000_PKT_ERR) {
            printk(KERN_ERR "dm9000_recv: rx error\n");
            LINK_STATS_INC(link.err);
            return;
        }

        if (!(rx_byte & DM9000_PKT_RDY)) {
            LINK_STATS_INC(link.err);
            return;
        }

        (netif_priv->rx_status)(dm9000, &rx_status, &rx_len);

        if (rx_len < 0x40) {
            printk(KERN_ERR "dm9000_drop: rx len error, rx len < 0x40\n");
            LINK_STATS_INC(link.lenerr);
        }

        if (rx_len > DM9000_PKT_MAX) {
            printk(KERN_ERR "dm9000_drop: rx len error, rx len > %d\n", DM9000_PKT_MAX);
            LINK_STATS_INC(link.lenerr);
        }

        rx_status >>= 8;

        if (rx_status & (RSR_FOE | RSR_CE | RSR_AE |
                         RSR_PLE | RSR_RWTO |
                         RSR_LCS | RSR_RF)) {

            if (rx_status & RSR_FOE) {
                printk(KERN_ERR "dm9000_drop: rx fifo error\n");
                LINK_STATS_INC(link.err);
            }

            if (rx_status & RSR_CE) {
                printk(KERN_ERR "dm9000_drop: rx crc error\n");
                LINK_STATS_INC(link.chkerr);
            }

            if (rx_status & RSR_RF) {
                printk(KERN_ERR "dm9000_drop: rx len error\n");
                LINK_STATS_INC(link.lenerr);
            }
        }

        netif_priv->dummy_inblk(dm9000, rx_len);

        LINK_STATS_INC(link.drop);

#ifdef LINK_SNMP_STAT
        snmp_inc_ifindiscards(netif);
#endif
    } while (rx_byte & DM9000_PKT_RDY);
}
/*********************************************************************************************************
** Function name:           dm9000_recv
** Descriptions:            DM9000 接收数据
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_recv(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    struct pbuf               *p, *q;
    u8_t                       reg;
    u8_t                       rx_byte;
    u16_t                      rx_len;
    u16_t                      rx_status;
    int                        good_pkt;
    err_t                      err;

    API_InterVectorDisable(netif_priv->irq);
    API_SemaphoreMPend(netif_priv->lock, LW_OPTION_WAIT_INFINITE);

    do {
        dm9000_io_read(dm9000, DM9000_MRCMDX);
        rx_byte = read8(dm9000->uiDataAddr) & 0x03;

        if (rx_byte & DM9000_PKT_ERR) {
            printk(KERN_ERR "dm9000_recv: rx error\n");
            LINK_STATS_INC(link.err);
            goto __exit;
        }

        if (!(rx_byte & DM9000_PKT_RDY)) {
            LINK_STATS_INC(link.err);
            goto __exit;
        }

        good_pkt = TRUE;

        (netif_priv->rx_status)(dm9000, &rx_status, &rx_len);

        if (rx_len < 0x40) {
            printk(KERN_ERR "dm9000_recv: rx len error, rx len < 0x40\n");
            LINK_STATS_INC(link.lenerr);
            good_pkt = FALSE;
        }

        if (rx_len > DM9000_PKT_MAX) {
            printk(KERN_ERR "dm9000_recv: rx len error, rx len > %d\n", DM9000_PKT_MAX);
            LINK_STATS_INC(link.lenerr);
            good_pkt = FALSE;
        }

        rx_status >>= 8;

        if (rx_status & (RSR_FOE | RSR_CE | RSR_AE |
                         RSR_PLE | RSR_RWTO |
                         RSR_LCS | RSR_RF)) {

            if (rx_status & RSR_FOE) {
                printk(KERN_ERR "dm9000_recv: rx fifo error\n");
                LINK_STATS_INC(link.err);
            }

            if (rx_status & RSR_CE) {
                printk(KERN_ERR "dm9000_recv: rx crc error\n");
                LINK_STATS_INC(link.chkerr);
            }

            if (rx_status & RSR_RF) {
                printk(KERN_ERR "dm9000_recv: rx len error\n");
                LINK_STATS_INC(link.lenerr);
            }

            good_pkt = FALSE;
        }

        if (good_pkt) {
#if ETH_PAD_SIZE
            rx_len += ETH_PAD_SIZE;
#endif

            if ((p = pbuf_alloc(PBUF_RAW, rx_len, PBUF_POOL)) != NULL) {
#if ETH_PAD_SIZE
                pbuf_header(p, -ETH_PAD_SIZE);
#endif

                for (q = p; q != NULL; q = q->next) {
                    (netif_priv->inblk)(dm9000, q->payload, q->len);
                }

#if ETH_PAD_SIZE
                pbuf_header(p, ETH_PAD_SIZE);
#endif

                LINK_STATS_INC(link.recv);
#ifdef LINK_SNMP_STAT
                snmp_add_ifinoctets(netif, rx_len);
                snmp_inc_ifinucastpkts(netif);
#endif

                err = netif->input(p, netif);
                if (err != ERR_OK) {
                    printk(KERN_ERR "dm9000_recv: packet input error, err = %d\n", err);
                    pbuf_free(p);
                    p = NULL;
                }
            } else {
#if ETH_PAD_SIZE
                rx_len -= ETH_PAD_SIZE;
#endif
                netif_priv->dummy_inblk(dm9000, rx_len);

                LINK_STATS_INC(link.memerr);
                LINK_STATS_INC(link.drop);
#ifdef LINK_SNMP_STAT
                snmp_inc_ifindiscards(netif);
#endif
            }
        } else {
            netif_priv->dummy_inblk(dm9000, rx_len);

            LINK_STATS_INC(link.drop);
#ifdef LINK_SNMP_STAT
            snmp_inc_ifindiscards(netif);
#endif
        }
    } while (rx_byte == DM9000_PKT_RDY);

    __exit:
    /*
     * 打开接收中断
     */
    reg  = dm9000_io_read(dm9000, DM9000_IMR);
    reg |= IMR_PRM;
    dm9000_io_write(dm9000, DM9000_IMR, reg);

    API_SemaphoreMPost(netif_priv->lock);
    API_InterVectorEnable(netif_priv->irq);
}
/*********************************************************************************************************
** Function name:           dm9000_output
** Descriptions:            DM9000 发送数据
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static err_t dm9000_output(struct netif *netif, struct pbuf *p)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    struct pbuf               *q;
    u16_t                      tx_len;
    int                        err;

    API_InterVectorDisable(netif_priv->irq);
    API_SemaphoreMPend(netif_priv->lock, LW_OPTION_WAIT_INFINITE);

    while (API_AtomicGet(&netif_priv->tx_pkt_cnt) > 1) {

        API_SemaphoreMPost(netif_priv->lock);
        API_InterVectorEnable(netif_priv->irq);

        err = API_SemaphoreBPend(netif_priv->tx_sync, 200);

        if (err == ERROR_THREAD_WAIT_TIMEOUT) {
            printk(KERN_ERR "dm9000_output: tx timeout!\n");
            return  (ERR_TIMEOUT);
        } else {
            API_InterVectorDisable(netif_priv->irq);
            API_SemaphoreMPend(netif_priv->lock, LW_OPTION_WAIT_INFINITE);
        }
    }

    tx_len  = p->tot_len;
#if ETH_PAD_SIZE
    tx_len -= ETH_PAD_SIZE;
#endif

#if ETH_PAD_SIZE
    pbuf_header(p, -ETH_PAD_SIZE);
#endif

    write8(DM9000_MWCMD, dm9000->uiIoAddr);

    for (q = p; q != NULL; q = q->next) {
        (netif_priv->outblk)(dm9000, q->payload, q->len);
    }

#if ETH_PAD_SIZE
    pbuf_header(p, ETH_PAD_SIZE);
#endif

    netif_priv->queue_pkt_len = tx_len;

    if (API_AtomicInc(&netif_priv->tx_pkt_cnt) == 1) {
        dm9000_io_write(dm9000, DM9000_TXPLL, (tx_len & 0xFF));
        dm9000_io_write(dm9000, DM9000_TXPLH, (tx_len >> 8) & 0xFF);
        dm9000_io_write(dm9000, DM9000_TCR, TCR_TXREQ);
    }

    API_SemaphoreMPost(netif_priv->lock);
    API_InterVectorEnable(netif_priv->irq);

    LINK_STATS_INC(link.xmit);
#ifdef LINK_SNMP_STAT
    snmp_add_ifoutoctets(netif, tx_len);
    snmp_inc_ifoutucastpkts(netif);
#endif

    return ERR_OK;
}
/*********************************************************************************************************
** Function name:           dm9000_watchdog
** Descriptions:            DM9000 看门狗，用于检测网线是否接通
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          中断返回值
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_watchdog(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    u16_t link;

    API_InterVectorDisable(netif_priv->irq);
    API_SemaphoreMPend(netif_priv->lock, LW_OPTION_WAIT_INFINITE);

    if ((dm9000_phy_read(dm9000, 1) & 0x20)) {

        if (!(netif_is_link_up(netif))) {

            link = dm9000_phy_read(dm9000, 17) >> 12;                   /*  see what we've got          */
            switch (link) {
            case 1:
                printk(KERN_INFO "dm9000_watchdog: operating at 10M half duplex mode\n");
                netif->link_speed = 10000000;                           /* 10Mbps                       */
                break;

            case 2:
                printk(KERN_INFO "dm9000_watchdog: operating at 10M full duplex mode\n");
                netif->link_speed = 10000000;                           /* 10Mbps                       */
                break;

            case 4:
                printk(KERN_INFO "dm9000_watchdog: operating at 100M half duplex mode\n");
                netif->link_speed = 100000000;                          /* 100Mbps                      */
                break;

            case 8:
                printk(KERN_INFO "dm9000_watchdog: operating at 100M full duplex mode\n");
                netif->link_speed = 100000000;                          /* 100Mbps                      */
                break;

            default:
                printk(KERN_INFO "dm9000_watchdog: operating at unknown 0x%02x mode\n", link);
                break;
            }

            netif_set_link_up(netif);
        }
    } else {
        if (netif_is_link_up(netif)) {

            netif_set_link_down(netif);

            printk(KERN_INFO "dm9000_watchdog: can not establish link\n");
        }
    }

    API_SemaphoreMPost(netif_priv->lock);
    API_InterVectorEnable(netif_priv->irq);

#ifdef LWIP_LINK_STAT
    netif->link_type      = snmp_ifType_ethernet_csmacd;
    netif->ts             = (UINT)API_TimeGet();
#endif
}
/*********************************************************************************************************
** Function name:           dm9000_send_done
** Descriptions:            DM9000 发送完成
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_send_done(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    u8_t                       tx_status;

    tx_status = dm9000_io_read(dm9000, DM9000_NSR);                     /*  Get TX status               */
    if (tx_status & (NSR_TX2END | NSR_TX1END)) {

        if (API_AtomicGet(&netif_priv->tx_pkt_cnt) > 0) {
            /*
             * Queue packet check & send
             */
            if (API_AtomicDec(&netif_priv->tx_pkt_cnt) > 0) {
                dm9000_io_write(dm9000, DM9000_TXPLL, (netif_priv->queue_pkt_len & 0xFF));
                dm9000_io_write(dm9000, DM9000_TXPLH, (netif_priv->queue_pkt_len >> 8) & 0xFF);
                dm9000_io_write(dm9000, DM9000_TCR, TCR_TXREQ);
            }

            API_SemaphoreBFlush(netif_priv->tx_sync, NULL);
        }
    }
}
/*********************************************************************************************************
** Function name:           dm9000_isr
** Descriptions:            DM9000 中断
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static void dm9000_isr(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    u8_t                       int_status;
    u8_t                       last_io;
    u8_t                       reg;

    /*
     * Save previous io register
     */
    last_io = read8(dm9000->uiIoAddr);

    /*
     * Disable all interrupts
     */
    reg = dm9000_io_read(dm9000, DM9000_IMR);
    dm9000_io_write(dm9000, DM9000_IMR, IMR_PAR);

    /*
     * Get DM9000 interrupt status
     */
    int_status = dm9000_io_read(dm9000, DM9000_ISR);                    /*  Get ISR status              */
    dm9000_io_write(dm9000, DM9000_ISR, int_status);                    /*  Clear ISR status            */

    if (int_status & ISR_ROS) {
        printk(KERN_ERR "dm9000_isr: rx overflow\n");
        LINK_STATS_INC(link.err);
    }

    if (int_status & ISR_ROOS) {
        printk(KERN_ERR "dm9000_isr: rx overflow counter overflow\n");
        LINK_STATS_INC(link.err);
    }

    /*
     * Received the coming packet
     */
    if (int_status & ISR_PRS) {
        if (netJobAdd((VOIDFUNCPTR)dm9000_recv,
                      netif,
                      0, 0, 0, 0, 0) == ERROR_NONE) {
            /*
             * 关闭接收中断
             */
            reg &= ~(IMR_PRM);
        } else {
            dm9000_drop(netif);
        }
    }

    /*
     * Transmit Interrupt check
     */
    if (int_status & ISR_PTS) {
        dm9000_send_done(netif);
    }

    /*
     * Re-enable interrupt mask
     */
    dm9000_io_write(dm9000, DM9000_IMR, reg);

    /*
     * Restore previous io register
     */
    write8(last_io, dm9000->uiIoAddr);
}
/*********************************************************************************************************
** Function name:           dm9000_eint_isr
** Descriptions:            DM9000 外部中断服务函数
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          中断返回值
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
static irqreturn_t dm9000_eint_isr(struct netif *netif, ULONG ulVector)
{
    struct dm9000_netif_priv  *netif_priv = netif->state;
    struct dm9000_data        *dm9000 = &netif_priv->dm9000;
    irqreturn_t                ret;

    ret = API_GpioSvrIrq(dm9000->uiEintGpio);
    if (ret == LW_IRQ_HANDLED) {
        API_GpioClearIrq(dm9000->uiEintGpio);
        dm9000_isr(netif);
    }

    return  (ret);
}
/*********************************************************************************************************
** Function name:           dm9000_netif_init
** Descriptions:            初始化以太网网络接口
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
err_t dm9000_netif_init(struct netif *netif)
{
    struct dm9000_netif_priv  *netif_priv;
    int                        ret;
    LW_OBJECT_HANDLE           handle;
    int                        irq;

    LWIP_ASSERT("netif != NULL", (netif != NULL));

    if (netif->state == NULL) {
        printk(KERN_ERR "dm9000_netif_init: invalid dm9000_data\n");
        return ERR_ARG;
    }

    netif_priv = (struct dm9000_netif_priv *)mem_malloc(sizeof(struct dm9000_netif_priv));
    if (netif_priv == NULL) {
        printk(KERN_ERR "dm9000_netif_init: out of memory\n");
        return ERR_MEM;
    }

#if LWIP_NETIF_HOSTNAME
    /*
     * Initialize interface hostname
     */
    netif->hostname = HOSTNAME;
#endif /* LWIP_NETIF_HOSTNAME */

    /*
     * Initialize the snmp variables and counters inside the struct netif.
     * The last argument should be replaced with your link speed, in units
     * of bits per second.
     */
    NETIF_INIT_SNMP(netif, snmp_ifType_ethernet_csmacd, 100000000);

    lib_memcpy(&netif_priv->dm9000,
               netif->state,
               sizeof(netif_priv->dm9000));

    netif->state   = netif_priv;
    netif->name[0] = IFNAME0;
    netif->name[1] = IFNAME1;

    /*
     * We directly use etharp_output() here to save a function call.
     * You can instead declare your own function an call etharp_output()
     * from it if you have to do some checks before sending (e.g. if link
     * is available...)
     */
    netif->output     = etharp_output;
    netif->linkoutput = dm9000_output;
    netif->output_ip6 = ethip6_output;

    /*
     * Set MAC hardware address length
     */
    netif->hwaddr_len = ETHARP_HWADDR_LEN;

    /*
     * Set MAC hardware address
     */
    lib_memcpy(netif->hwaddr, netif_priv->dm9000.ucMacAddr, 6);

    /*
     * Set Maximum transfer unit
     */
    netif->mtu = 1500;

    /*
     * Set Device capabilities
     */
    netif->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_ETHERNET;

    netif_priv->queue_pkt_len = 0;
    API_AtomicSet(0, &netif_priv->tx_pkt_cnt);

    handle = API_SemaphoreMCreate("ethif_lock",
                                  0,
                                  LW_OPTION_INHERIT_PRIORITY | LW_OPTION_OBJECT_GLOBAL,
                                  &netif_priv->lock);
    if (handle == LW_HANDLE_INVALID) {
        printk(KERN_ERR "dm9000_netif_init: out of memory\n");
        return ERR_MEM;
    }

    handle = API_SemaphoreBCreate("ethif_tx_sync",
                                  0,
                                  LW_OPTION_WAIT_FIFO | LW_OPTION_OBJECT_GLOBAL,
                                  &netif_priv->tx_sync);
    if (handle == LW_HANDLE_INVALID) {
        printk(KERN_ERR "dm9000_netif_init: out of memory\n");
        return ERR_MEM;
    }

    netif_priv->watchdog = API_TimerCreate("ethif_watchdog",
                                           LW_OPTION_ITIMER | LW_OPTION_OBJECT_GLOBAL,
                                           LW_NULL);
    if (netif_priv->watchdog == LW_HANDLE_INVALID) {
        printk(KERN_ERR "dm9000_netif_init: out of memory\n");
        return ERR_MEM;
    }

    if (netif_priv->dm9000.pfuncInit) {
        netif_priv->dm9000.pfuncInit();
    }

    netif_priv->dm9000.uiBaseAddr = (addr_t)API_VmmIoRemapNocache((PVOID)netif_priv->dm9000.uiBaseAddr,
                                                                  LW_CFG_VMM_PAGE_SIZE);
    if (netif_priv->dm9000.uiBaseAddr == (addr_t)LW_NULL) {
        printk(KERN_ERR "dm9000_netif_init: failed to remap dm9000 memory space\n");
        return ERR_MEM;
    }

    netif_priv->dm9000.uiDataAddr += netif_priv->dm9000.uiBaseAddr;
    netif_priv->dm9000.uiIoAddr   += netif_priv->dm9000.uiBaseAddr;

    /*
     * 初始化 DM9000 芯片
     */
    ret = dm9000_init(netif);
    if (ret != 0) {
        printk(KERN_ERR "dm9000_netif_init: failed to init dm9000\n");
        return ERR_IF;
    }

    ret = API_GpioRequestOne(netif_priv->dm9000.uiEintGpio,
                             LW_GPIOF_OUT_INIT_LOW,
                             "dm9000_eint");
    if (ret != ERROR_NONE) {
        printk(KERN_ERR "dm9000_netif_init: failed to request dm9000 gpio\n");
        return ERR_IF;
    }

    irq = API_GpioSetupIrq(netif_priv->dm9000.uiEintGpio,
                           LW_TRUE,
                           1);
    if (irq < 0) {
        printk(KERN_ERR "dm9000_netif_init: failed to setup dm9000 extern interrupt\n");
        return ERR_IF;
    }
    netif_priv->irq = irq;

    API_InterVectorConnect((ULONG)irq,
                           (PINT_SVR_ROUTINE)dm9000_eint_isr,
                           (PVOID)netif, "dm9000_isr");

    API_InterVectorEnable((ULONG)irq);

    API_TimerStart(netif_priv->watchdog,
                   DM9000_WATCHDOG_CNT,
                   LW_OPTION_AUTO_RESTART,
                   (PTIMER_CALLBACK_ROUTINE)dm9000_watchdog,
                   (PVOID)netif);

    return ERR_OK;
}
/*********************************************************************************************************
** END FILE
*********************************************************************************************************/
