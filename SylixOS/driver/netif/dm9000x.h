/*********************************************************************************************************
**
**                                    中国软件开源组织
**
**                                   嵌入式实时操作系统
**
**                                SylixOS(TM)  LW : long wing
**
**                               Copyright All Rights Reserved
**
**--------------文件信息--------------------------------------------------------------------------------
**
** 文   件   名: dm9000x.h
**
** 创   建   人: JiaoJinXing
**
** 文件创建日期: 2012 年 03 月 03 日
**
** 描        述: DM9000X 以太网芯片驱动.
*********************************************************************************************************/
#ifndef DM9000X_H_
#define DM9000X_H_

typedef struct dm9000_data {
    VOID            (*pfuncInit)(VOID);
    addr_t            uiBaseAddr;
    addr_t            uiIoAddr;
    addr_t            uiDataAddr;
    u32_t             uiEintGpio;
    u8_t              ucMacAddr[6];
} DM9000_DATA;

/*********************************************************************************************************
** Function name:           dm9000_netif_init
** Descriptions:            初始化以太网网络接口
** input parameters:        NONE
** output parameters:       NONE
** Returned value:          NONE
** Created by:              JiaoJinXing
** Created Date:            2012/11/30
**--------------------------------------------------------------------------------------------------------
** Modified by:
** Modified date:
**--------------------------------------------------------------------------------------------------------
*********************************************************************************************************/
err_t dm9000_netif_init(struct netif *netif);

#endif
/*********************************************************************************************************
** END FILE
*********************************************************************************************************/
